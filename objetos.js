var superheroe = {
    nombre: 'Peter Parker',
    edad: 17,
    esSuperHeroe: true,
}
/**
 * Formas de acceder a un objeto ->
 * dot notation: notacion de puntos, es decir: superheroe.nombre
 * bracket notation: superhepre['nombre]
 */

var nombreDelCampoEdad = 'edad';

console.log('Este super heore se llama:  ', superheroe.nombre);
console.log('Y tiene  ', + superheroe[nombreDelCampoEdad] + ' años');

// objeto con nombre casa propiedades : vasos (4), platos (5).
// En mi casa tengo 4 vasos y 5 platos.

var casa = {
    vasos: 4,
    platos: 5,
}


console.log('Es mi casa y tengo: '+ casa.vasos+ " vasos y " +casa.platos+ " platos");

console.log('Es mi casa y tengo: '+ casa['vasos']+ " vasos y " +casa["platos"]+ " platos");



