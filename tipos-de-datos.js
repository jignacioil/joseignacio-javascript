var nombre = 'Pepe';

console.log(nombre);

/**
 * NUMBER
 */
var edad=28;
var numeroPi= 3.14;
console.log(edad);
console.log(numeroPi);

var concatenado = 'upgrade' + 5 + 3;
console.log(concatenado);

var concatenado = 'upgrade' + (5 + 3);
console.log(concatenado);

console.log(edad + numeroPi);

var num3 =3;
console.log(num3);
/**
 * BOOLEAN
 */

var tengoEfectivo = true;
var tengoTarjeta = false;

var puedoPagar = tengoEfectivo || tengoTarjeta // operador logico: OR
console.log('puedo pagar:  ', puedoPagar);
// ---

var tengoCoche = false;
var tengoCarnet = true;

var puedoConducir = tengoCoche && tengoCarnet;  // operador logico: AND
console.log('puedo conducir:  ', puedoConducir);

// operador logico NOT

var soyProgramador = true;
console.log(!soyProgramador);

var tengoDinero = true;
var meDaMiedoVolar = true;

// Puedo ir a Mexico si tengo dinero y NO me da miedo volar
var puedoIrAMexico = tengoDinero && !meDaMiedoVolar;
console.log(puedoIrAMexico);

var meTomoUnTranquilizante = true;
// Puedo ir a Mexico si tengo dinero y NO me da miedo volar o me tomo un tranquilizante
var puedoIrAMexico = tengoDinero && (!meDaMiedoVolar || meTomoUnTranquilizante);
console.log(puedoIrAMexico);